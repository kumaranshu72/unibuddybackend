import axios from 'axios'

/**
 * This object store the basic configuration of the axios object
 */
const instance = axios.create({
  baseURL: 'https://jsonmock.hackerrank.com/api',
})

/**
 * This function return the response from remote server
 * @param {Object} config
 */
function fetchResponse(config: any) {
  return instance(config)
    .then(response => {
      const { data } = response
      return data
    })
    .catch(error => {
      throw error
    })
}
/**
 * This method process the REST Api request
 * @param {Object} config The config object required for axios which can be understood
 * from axios documentaion
 */
export default function api(config:any) {
  return fetchResponse(config)
}
