import api from './api'
import { get, range, each, pick, map,sortBy } from 'lodash'

export const getMovie = (title: string, pageNo = 1) => (
  api({
    method: 'get',
    url: `/movies/search/?Title=${title}&page=${pageNo}`,
  })
)

export const getAllMovies = async (title: string) => {
  const result = []
  const movie = await getMovie(title)
  const totalPages = get(movie, 'total_pages', 1)
  const data = get(movie, 'data', [])
  result.push(...pickTitle(data))
  const rangeVal = range(2, totalPages+1)
  const requests: any = []
  each(rangeVal, (val: any, ind: any) => {
    requests.push(getMovie(title, val))
  })
  const res = await Promise.all(requests)
  map(res, (item: any,index: any) => {
    const data = get(item, 'data', [])
    result.push(...pickTitle(data))
  })
  return sortBy(result)
}

const pickTitle = (data: any) => {
  return map(data, (item, index) => {
    const filData = pick(item , ['Title'])
    return get(filData, 'Title', '')
  })
}