import { Request, Response } from 'express'
import { get } from 'lodash'

import { getAllMovies } from '../utils/helper'

export const movies = async (req: Request, res: Response) => {
  const movieTitle = get(req, 'query.title', '')
  const movieData = await getAllMovies(movieTitle)
  res.send(movieData.join("</br>"))
}
