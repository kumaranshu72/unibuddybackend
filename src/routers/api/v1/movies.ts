import { Router, Request, Response } from 'express'

import {movies} from '../../../controller'

const router: Router = Router()

router.route('/movies')
      .get(movies)

export default router