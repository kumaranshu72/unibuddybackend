import { Router } from 'express'

import movies from './movies'

const router: Router = Router()

router.use('/', movies)

export default router