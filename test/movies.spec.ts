import { expect } from 'chai'
import 'mocha'
import request from 'supertest'

import app from '../src/app'


describe(`GET /api/v1/movies?title=ab - Testing Movies API`, () => {

  it('should return 200 OK and with list of movies', () => {
    return request(app)
           .get('/api/v1/movies?title=ab')
           .expect(200)
  })

})
